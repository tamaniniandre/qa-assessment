/// <reference types="cypress" />

import MainPageElements from '../pageelements/MainPageElements'

export default class MainPageActions {


    constructor() {

        globalThis.mainpageelements = new MainPageElements()
    }

    navigateToNewsPage() {

        mainpageelements.navigateToMainPageNews();

    }

    searchPostByText(text) {

        mainpageelements.searchBtnElement().click();
        mainpageelements.searchInputElement().type(text).type('{enter}');

    }

    findNewsByTitle(title) {

       return mainpageelements.findNewsResultByTittle(title)

    }


}