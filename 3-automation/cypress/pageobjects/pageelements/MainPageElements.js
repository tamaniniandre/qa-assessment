/// <reference types="cypress" />


const or = require("../../../locators.json")

export default class MainPageElements{

    navigateToMainPageNews(){

        cy.visit('/');
        
    }

    searchBtnElement(){

        return cy.get(or.mainpage.idSeachButton)

    }

    searchInputElement(){

        return cy.get(or.mainpage.idSearchInput)

    }

    findNewsResultByTittle(title){

        return cy.xpath(or.mainpage.xpathFindNewsByTitle.replace("{{title}}",title))

    }

}
