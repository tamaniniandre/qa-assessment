Feature: Searching at the Main Page

    I want to validate the Search at the news Main Page

    Scenario: Search a post in the news website
        Given I access the news website
        When I search for a post using a existent title
        Then the news website must return the searched post in a results page
