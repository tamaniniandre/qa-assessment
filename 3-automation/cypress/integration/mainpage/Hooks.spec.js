
before(() => {
    Cypress.on('uncaught:exception', (err, runnable) => {
        return false;
      });

    cy.fixture('testdata').then((data)=>{

        globalThis.data = data

    });
  })