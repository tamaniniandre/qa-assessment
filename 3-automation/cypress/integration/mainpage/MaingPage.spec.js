import MainPageActions from '../../pageobjects/pageactions/MainPageActions'

const mainPage = new MainPageActions()

Given('I access the news website',()=>{

    mainPage.navigateToNewsPage();
})

When('I search for a post using a existent title',()=>{

    mainPage.searchPostByText(data.titleSearch);
})

Then('the news website must return the searched post in a results page',()=>{

    mainPage.findNewsByTitle(data.titleSearch).should('exist');
    mainPage.findNewsByTitle(data.titleSearch).should('be.visible');
    mainPage.findNewsByTitle(data.titleSearch).should('have.length', 1);

})