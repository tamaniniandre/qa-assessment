# qa-assessement

QA-Assessment: André Tamanini

## How to execute

# Install the dependencies

- Install the node version v17.9.0 or higher (https://nodejs.org/en/download/)
- Install VSCode (https://code.visualstudio.com/download)
- Clone the repository on your machine
- Open it in VSCode
- Install the extension "Cucumber (Gherkin) Full Support" in VSCode (not mandatory) 
- Open the terminal and execute:
- - npm install
- - npm install --save-dev
- - npm install -g allure-commandline --save-dev (to generate allure reports)

## Running the tests
- Open the terminal and execute according to your choice:
- - npm run launchcypress (to open the cypress plugin and execute the test on your screen)
- - npm run execute-test-headless (to execute the tests in headless browser)
- - npm run execute-tests-generate-report (to execute the tests in headless browser and generate allure report)


## Evidences Reports
- Videos
- - After each execution in headless mode: the video evidence will be saved at cypress/videos
- Reports
- - After each execution in headless mode: the report html will be saved at cypress/reports/testresults.html
- - After each execution in headless mode and generate report allure: the report will be automatically opened in your browser